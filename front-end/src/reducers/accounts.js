import { FETCH_ALL } from "../constants/actionTypes";
const accounts = (accounts = [], action) => {
    switch(action.type){
        case FETCH_ALL:
            return action.payload;
        default:
            return accounts;
    }

}
export default accounts