import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import './App.css';

// components
import Header from './components/Header/Header';
import Hero from './components/Hero/Hero'
import Footer from './components/Footer/Footer'
import { getAccounts } from './actions/accounts';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAccounts())
  }, [dispatch]);

  return (
    <>
      <Header />
      <Hero />
      <Footer />
    </>
  )
}
export default App;
