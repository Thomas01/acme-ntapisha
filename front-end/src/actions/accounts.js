import { FETCH_ALL } from '../constants/actionTypes';
import * as api from '../api';

// Action Creators
export const getAccounts = () => async (dispatch) => {
    try {
        const { data } = await api.fetchAccount();
        dispatch({type: FETCH_ALL, payload: data});
    } catch (error){
        console.log(error)
    }
}

