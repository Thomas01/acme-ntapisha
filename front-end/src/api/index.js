import axios from 'axios';

const url = 'http://localhost:8080/api/accounts';

export const fetchAccount = () => axios.get(url);
