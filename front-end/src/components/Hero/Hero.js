import React from 'react'
import { useSelector} from 'react-redux';
//CSS
import { CircularProgress } from '@material-ui/core';
import './hero.css';
//Component
import DataTable from './DataTable/DataTable'


const Hero = () => {
    const accounts = useSelector((state) => state.accounts)
  
  return (
      !accounts.length ? <CircularProgress /> : (
        <div className = "cover_hero">
           <div>
              <DataTable accounts = {accounts}  />
          </div>
        </div>
      ) 
  )
}
export default Hero;