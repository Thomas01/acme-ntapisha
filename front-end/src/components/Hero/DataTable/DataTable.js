import React, { useState, useEffect } from 'react'

//Material UI
import Button from '@material-ui/core/Button'

//CSS
import './dataTable.css';


const DataTable = ({accounts}) => {

  const [ overDraft_Limit, setOverDraftLimit ] = useState([]);
  const [ success, setSuccess ] = useState(false);
  const [ message, setMessage ] = useState('');

 // remove alert box after 3 sec
  useEffect(() => {
    const timeout = setTimeout(() => {
       setSuccess(false);
     }, 3000);

     return () => clearTimeout(timeout);
   },[success]);

  
  // Total Balance
  const getBalance = (accounts) => {
      if (!accounts){
          return null
      } else {
          const balances = accounts.map((account) => parseInt(account.balance));
          const total = balances.reduce((balance, index) => balance + index);
          return <div>{`ZAR ${total}`}</div>
      } 
  }
// Driver code for balance
  let totalResult = getBalance(accounts)

  // Business rules
  const Validate = (account) => {
      let WithdrawAmount ;
   if (account.account_type === "cheque" && account.balance === 0 && account.balance !== overDraft_Limit) {
        setOverDraftLimit(-500)
      } else if (account.account_type === "cheque" && account.balance <= overDraft_Limit){
        setSuccess(true);
        setMessage('You have reached you limit !')  
      } else if( account.account_type === "savings" && WithdrawAmount > account.balance){
        setSuccess(true);
        setMessage('Insafient funds ! ')  
      } else {
        setSuccess(true);
        setMessage('Success !')  
    }
 }

 return (
    <div>  
            <div className ="header_cover"> 
                <h1 className ="table_header">Account List</h1>

                  { success && (

                        <div className ="alert_cover">
                        <span className = "alart_Message">{ message }</span>
                        </div>
                      
                  )}
                 
            </div>

 <table id="accounts">
   <thead>
    <tr>
      <th>Account Number</th>
      <th>Account Type</th>
      <th>Balance</th>
    </tr>
   </thead>
  <tbody>
    { accounts.map((account, index) => {
        if(account.account_type === "savings" && account.balance <= -20 ){
          return <tr key = {index}><td>{account.account_number}</td><td>{account.account_type}</td><td>{account.balance}</td><td><Button style={{textTransform: 'none'}} disabled >Withdraw</Button></td></tr>
        }  else {
        return <tr key = {index}><td>{account.account_number}</td><td>{account.account_type}</td><td>{account.balance}</td><td><button onClick = {()=>Validate(account)}className="button muted-button">Withdraw</button></td></tr>
        }
    })} 
  </tbody>
  </table>

      <div className ="total">
        <span className ="balance_lebel">Balance:</span>
        <span className ="balance_total">{totalResult}</span>
      </div>

   </div>
      )
} 

export default DataTable;
